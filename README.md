# Description:

This will be my personal website. I figured I should put one up there even though it will be very basic. I will keep adding features to it whenever I get the chance. 

This website will be powered by Hugo, a static site generator written in the Go Programming Language.

Hugo: Making websites fun again.



# Licenses:

Content will be released under [Creative Commons Share-Alike 2.5 License](http://creativecommons.org/licenses/by-sa/2.5/)

Anything done with source code will be released under the [MIT License](http://opensource.org/licenses/MIT)